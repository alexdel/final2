

def mirror(image: dict) -> dict:
    width, height = image['width'], image['height']
    pixels = image['pixels']
    mirrored_pixels = []
    
    for x in range(height):
        
        for y in range(width):
            mirrored_pixels.append(pixels[(height - 1 - x) * width + y])
    
    return {'width': width, 'height': height, 'pixels': mirrored_pixels}


def grayscale(image: dict) -> dict:
    width, height = image['width'], image['height']
    pixels = image['pixels']
    grayscale_pixels = []
    
    for pixel in pixels:
        gray_value = sum(pixel) // 3
        grayscale_pixels.append((gray_value, gray_value, gray_value))
    
    return {'width': width, 'height': height, 'pixels': grayscale_pixels}


def blur(image: dict) -> dict:
     width, height = image['width'], image['height']
     pixels = image['pixels']
     blurred_pixels = pixels.copy()

     for x in range(1, height - 1):
         
         for y in range(1, width - 1):
             sum_r = sum_g = sum_b = 0

             for dx in range(-1, 2):
                 
                 for dy in range(-1, 2):
                     index = (x + dx) * width + (y + dy)
                     pixel = pixels[index]
                     sum_r += pixel[0]
                     sum_g += pixel[1]
                     sum_b += pixel[2]

             blurred_pixel = (
                 sum_r // 9,
                 sum_g // 9,
                 sum_b // 9
             )
             blurred_pixels[y * width + x] = blurred_pixel
     
     return {'width': width, 'height': height, 'pixels': blurred_pixels}


def change_colors(image: dict, original: list[tuple[int, int, int]], change: list[tuple[int, int, int]]) -> dict:
    width, height = image['width'], image['height']
    pixels = image['pixels']
    color_map = dict(zip(original, change))
    changed_pixels = [color_map.get(pixel, pixel) for pixel in pixels]
    
    return {'width': width, 'height': height, 'pixels': changed_pixels}


def rotate(image: dict, direction: str) -> dict:
    width, height = image['width'], image['height']
    pixels = image['pixels']
    
    if direction == 'right':
        new_pixels = [pixels[(height - 1 - y) * width + x] for x in range(height) for y in range(width)]
        
        return {'width': height, 'height': width, 'pixels': new_pixels}
    
    elif direction == 'left':
        new_pixels = [pixels[y * width + (width - 1 - x)] for x in range(height) for y in range(width)]
        return {'width': height, 'height': width, 'pixels': new_pixels}
    
    else:
        raise ValueError("Direction must be 'left' or 'right'")



def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    old_width, old_height = image['width'], image['height']
    new_width, new_height = old_width + horizontal, old_height + vertical
    old_pixels = image['pixels']
    new_pixels = [(0, 0, 0)] * (new_width * new_height)

    for x in range(old_height):
        
        for y in range(old_width):
            new_y, new_x = y + horizontal, x + vertical
            
            if 0 <= new_y < new_width and 0 <= new_x < new_height:
                new_pixels[new_x * new_width + new_y] = old_pixels[x * old_width + y]
    
    return {'width': new_width, 'height': new_height, 'pixels': new_pixels}


def crop(image: dict, y: int, x: int, width: int, height: int) -> dict:
    original_width, original_height = image['width'], image['height']
    pixels = image['pixels']
    cropped_pixels = [
        pixels[(x + j) * original_width + (y + i)]
        for j in range(height) for i in range(width)
        if 0 <= y + i < original_width and 0 <= x + j < original_height ]
    
    return {'width': width, 'height': height, 'pixels': cropped_pixels}


def filter(image: dict, r: float, g: float, b: float) -> dict:
    width, height = image['width'], image['height']
    pixels = image['pixels']
    filtered_pixels = [
        (
            min(int(pixel[0] * r), 255),
            min(int(pixel[1] * g), 255),
            min(int(pixel[2] * b), 255)
        ) for pixel in pixels ]
    
    return {'width': width, 'height': height, 'pixels': filtered_pixels}

if __name__ == '__main__': pass
